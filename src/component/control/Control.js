import './control.css'
import React, { Component } from 'react';
import { Row, Col  } from 'antd';
import ReactDOM from 'react-dom';
import {
  PlayCircleFilled,
  PauseCircleFilled,
  CustomerServiceFilled,
  PlusCircleFilled,
  MinusCircleFilled,
  SoundFilled,
  DislikeFilled,
  LikeFilled,
  RotateLeftOutlined

} from '@ant-design/icons';
import { video} from 'react-html5video';
import 'react-html5video/dist/styles.css';
import Axios from 'axios';

const axios = Axios.create();

export class Control extends Component{
	constructor(props){
		// 父类构造函数的引用
		super(props)
		// this.state是当前class组件私有数据
		this.state= {
            video: {
                id: 0,               
                title: "",
                unLike: 0,
                like: 0,
                volume: 0.5,
                url: "",
                status: 1,
                controls: false,
                pause:true,
                currentTime:'0:00',
                duration:'0:00'                  
            },
            playOrPaseButton: {},
            muteOrUnmutebutton: {},            
            playFlag: false,
            muteFlag: false,            
            initBaseData: ()=>{
                let video = this.state.getMyvideo();
                video.volume= 0.5;
                video.load();
                video.pause();
                this.state.playFlag = false;
                this.state.muteFlag = false; 
                this.state.playOrPaseButton={
                  playOrPaseIcon: <PlayCircleFilled />,
                  playOrPaseIconClass: "spanGreenColor",
                };
                this.state.muteOrUnmutebutton={
                  muteIcon: <CustomerServiceFilled />,
                  muteIconClass: "spanBlackColor",
                }   ;             
                this.setState(this.state);

                axios.get('http://localhost:3003/vedio/'+this.props.match.params.id)
                .then((response) => {
                  this.state.video.id = response.data.id;
                  this.state.video.title = response.data.title;
                  this.state.video.unLike = response.data.unLike;
                  this.state.video.like = response.data.like;
                  this.state.video.url = response.data.url
                  this.setState(this.state);
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            getMyvideo: ()=>{
                return   ReactDOM.findDOMNode(document.getElementById('myVideo'));      
            },
            putLike: ()=>{
              this.state.video.like += 1;
              axios.put('http://localhost:3003/vedio/'+this.props.match.params.id, this.state.video)
                .then((response) => {
                    this.setState(this.state)
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            putUnLike: ()=>{
              this.state.video.unLike += 1;
              console.log("id:"+this.props.match.params.id)
              axios.put('http://localhost:3003/vedio/'+this.props.match.params.id, this.state.video)
                .then((response) => {
                    this.setState(this.state)
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            palyOrPausevideo: ()=>{
                
                let video = this.state.getMyvideo();
                //console.log("palyOrPausevideo:"+video);
                if( this.state.playFlag){
                    video.pause();
                    this.setState({                        
                        playOrPaseButton: {
                            playOrPaseIcon: <PlayCircleFilled />,
                            playOrPaseIconClass: "spanGreenColor"
                        },
                        playFlag: false
                    });
                }else{
                    video.play();
                    this.setState({
                        playOrPaseButton: {
                            playOrPaseIcon: <PauseCircleFilled />,
                            playOrPaseIconClass: "spanRedColor"
                        },
                        playFlag: true
                    });
                }
            },
            rePlay: ()=>{
                let video = this.state.getMyvideo();
                video.load();
            },
            muteOrUnmute: ()=>{
                if(this.state.muteFlag){
                    this.state.getMyvideo().muted = false;
                    //this.setState.controlvideoIcon.muteIconClass="spanGreyColor"
                    this.setState({
                        muteFlag: false,
                        muteOrUnmutebutton: {
                            muteIcon: <CustomerServiceFilled />,
                            muteIconClass: "spanBlackColor ",
                        },
                    })
                }else{
                    this.state.getMyvideo().muted = true;
                    this.setState({
                        muteFlag: true,
                        muteOrUnmutebutton: {
                            muteIcon: <CustomerServiceFilled />,
                            muteIconClass: "spanGreyColor ",
                        },
                    })
                }

            },
            plusVolume: ()=>{
                let video = this.state.getMyvideo();
                //console.log("volume-video:"+video.volume);
                video.volume += video.volume === 1 ? 0 : 0.1;                
                video.volume = parseFloat(video.volume).toFixed(1);
                this.state.video.volume=video.volume ;
                this.setState(this.state);
                
            },
            miusVolume: ()=>{
              let video = this.state.getMyvideo();
              //console.log("volume-video:"+video.volume);
              video.volume -= video.volume === 0 ? 0 : 0.1;                
              video.volume = parseFloat(video.volume).toFixed(1);
              this.state.video.volume=video.volume ;
              this.setState(this.state);
            },
            loadedmetadata: ()=>{
                //console.log("loadedmetadata");
                let video = this.state.getMyvideo();
                this.state.video.duration = this.state.initTimeLength(video.duration) ;
                this.state.video.currentTime = this.state.initTimeLength(video.currentTime);
                this.setState(this.state);
            },
            timeupdata:()=>{
              //console.log("timupdata")
              let video = this.state.getMyvideo();
              this.state.video.currentTime =this.state.initTimeLength(video.currentTime);
              let durationProgress=video.duration;
              let currentTimeProgress=video.currentTime;
              // 求当前播放时长的进度，从而显示出来进度条
              this.state.setCurrentProgressWidth(currentTimeProgress, durationProgress);
              this.setState(this.state);
            },
            setCurrentProgressWidth: (currentTimeProgress,durationProgress)=>{
              let currentWidth=100*(currentTimeProgress/durationProgress);
              const drawBar = ReactDOM.findDOMNode(document.getElementById('drawBar'));
              const durationBar = ReactDOM.findDOMNode(document.getElementById("durationBar"));
              const currentBar = ReactDOM.findDOMNode(document.getElementById('currentBar'));
              let currentLeft=currentWidth-((drawBar.offsetWidth/2)/durationBar.offsetWidth*100);
              currentBar.style.width = currentWidth > 0 ? currentWidth.toFixed(1)+'%' : '0px';
              drawBar.style.left = currentLeft > 0 ? currentLeft.toFixed(1)+'%': '0px';
            },
            initTimeLength: (timeLength)=> { // 根据秒数格式化时间
                //console.log("initTimeLength");
                timeLength = parseInt(timeLength);
                let second = timeLength % 60;
                let minute = (timeLength - second) / 60;
                return (minute<10?"0"+minute:minute)+":"+(second<10?"0"+second:second);
            }
		  }
    }
    
    componentDidMount() {
        console.log("componentDidMount");
        this.state.initBaseData();

        this.props.history.listen(route => {
          console.log("route:"+route)
          this.state.initBaseData();
          this.setState(this.state);
          console.log("url:"+this.state.video.url)
        })
    }

	//render是一个生命周期中的函数
	render(){
        return (
            <Row tyle="flex" justify="space-around" align="middle">
            <Col span={16} className="height-500 ">
              <Row>
                <Col span={24}>
                  <h2>{this.state.video.title}</h2> 
                </Col>
              </Row>
              <Row>
                <Col span={24} className="backgroudGrey">
                  <Row >
                    <Col span={20} offset={2} align="middle"  style={{ marginTop: '40px' }} >
                        {/* <video autoPlay  controls={this.state.video.controls} id="myVideo" className="vedio_size" 
                            onLoadedMetadata={this.state.loadedmetadata}
                            onTimeUpdate={this.state.timeupdata}>
                            <source src={this.state.video.url} type="video/webm" />
                        </video> */}
                        <Video controls={this.state.video.controls} loadedmetadata={this.state.loadedmetadata}
                          timeupdata={this.state.timeupdata} url={this.state.video.url} /> 
                    </Col>
                  </Row>
                  <Row>
                    <Col span={7} offset={1} align="middle">
                      {/* <暂停/播放键 */}
                      <div >
                        <span className={this.state.playOrPaseButton.playOrPaseIconClass} onClick={this.state.palyOrPausevideo} >{ this.state.playOrPaseButton.playOrPaseIcon}</span>
                        <span className="spanYellowColor marginLeft10" onClick={this.state.rePlay} ><RotateLeftOutlined /></span>
                        <span className={this.state.muteOrUnmutebutton.muteIconClass+" marginLeft10" } onClick={this.state.muteOrUnmute}>{this.state.muteOrUnmutebutton.muteIcon}</span>
                        <span className="spanBlackColor marginLeft10" onClick={this.state.plusVolume}><PlusCircleFilled /></span>
                        <span className="spanBlackColor marginLeft10" onClick={this.state.miusVolume} ><MinusCircleFilled /></span>
                        <span className="spanBlackColor marginLeft10"> <SoundFilled /> {this.state.video.volume * 10}</span>
                      </div>
                    </Col>
                    <Col span={10} align="middle" className="marginTop10" >
                      <Row  >
                        <Col span={2} offset={2} className="fontSize16"> {this.state.video.currentTime}</Col>
                        <Col span={12} className="marginLeft10"> 
                          {/* <!--总视频长度进度条--> */}
                          <div className="durationbar" id="durationBar">
                            {/* <!--缓冲进度条--> */}
                            <div className="bufferbar" id="bufferBar"></div>
                            {/* <!--正在播放进度条--> */}
                            <div className="currentbar" id="currentBar"></div>
                            <div className="drawbar" id="drawBar"></div>
                          </div>
                        </Col>
                        <Col span={2}  className="fontSize16 marginLeft10">{this.state.video.duration}</Col>
                      </Row>
                    </Col>
                    <Col span={5}>
                        <span className="spanGreenColor marginLeft10" onClick={this.state.putLike}><LikeFilled />{this.state.video.like}</span>
                        <span className="spanRedColor marginLeft10" onClick={this.state.putUnLike}><DislikeFilled /> {this.state.video.unLike}</span>
                    </Col>
                  </Row>   
                </Col>
              </Row>
            </Col>
          </Row>
        );
	}
}
function Video (props) {
  return <video autoPlay  controls={props.controls} id="myVideo" className="vedio_size" 
          onLoadedMetadata={props.loadedmetadata}
          onTimeUpdate={props.timeupdata}>
          <source src={props.url} type="video/webm" />
        </video>;
}
export default Control
