import {Row, Col  } from 'antd';
import React, { Component } from 'react';

export class HeaderContent extends Component{
	//render是一个生命周期中的函数
	render(){
        return (
            <Row>
                <Col span={8} offset={10}> <h1 className="fontSize32"> 002-React 作业</h1> </Col>
            </Row>
        ) ;
	}
}

export default HeaderContent
