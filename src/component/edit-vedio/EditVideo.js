
import React, {Component } from 'react';
import 'antd/dist/antd.css';
import { Table, Input, Form , Button } from 'antd';
import ReactDOM from 'react-dom';

import Axios from 'axios';

const axios = Axios.create();

//子组件
export class EditVideo extends Component{
	constructor(props){
		// 父类构造函数的引用
		super(props)
		// this.state是当前class组件私有数据
		this.state= {
			data: [],
			columns: [
				{
				  title: 'No',
				  dataIndex: 'id',
				  width: '25%',
				  editable: true,
				},
				{
				  title: 'Title',
				  dataIndex: 'title',
				  width: '15%',
          editable: false,
          render: (_, record)=>{
            return (
              
              this.state.columns[2].editable ? (
                <Form.Item
                  name={this.state.dataIndex}
                  style={{margin: 0,}}
                  rules={[ { required: true,  },	]}
                >
                <Input defaultValue={record.title} id={"title"+record.id}/>
                </Form.Item>
              ) : (
                record.title
              )
            );
          }  
				},
				{
				  title: 'URL',
				  dataIndex: 'url',
				  width: '40%',
          editable: false,
          render: (_, record)=>{
            return (
              this.state.columns[2].editable ? (
                <Form.Item
                  name={this.state.dataIndex}
                  style={{margin: 0,}}
                  rules={[ { required: true,  },	]}
                >
                <Input defaultValue={record.url} id={"url"+record.id }/>
                </Form.Item>
              ) : (
                record.url
              )
            );
          }          
				},
				{
          title: 'Action',
          editable: true,
					render: (_, record) => {
            return (
            <div>
              <a disabled={this.state.columns[3].editable } onClick={() => this.state.save(record)}> save </a>
              <a disabled={this.state.columns[3].editable || record.status === 1 } onClick={() => this.state.approve(record)}  style={{marginLeft: 10,}}>Approve</a>  
              <a disabled={this.state.columns[3].editable} onClick={() => this.state.delete(record)} style={{marginLeft: 10,}}>Delete</a>  
            </div>)
                    
					},
				}
      ],
      delete: (record)=>{
        record.status=-1;
        axios.put('http://localhost:3003/vedio/'+record.id,record)
      	.then((response) => {       
          this.state.getVideos();
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      approve: (record)=>{
        console.log("title:"+record.title)
        record.status=1;
        axios.put('http://localhost:3003/vedio/'+record.id,record)
      	.then((response) => {       
          console.log("save:"+response.data)
          this.state.getVideos();
        })
        .catch(function (error) {
            console.log(error);
        });
      },
			isEditing: ()=>{},
			save: (record)=>{
        let titleValue= ReactDOM.findDOMNode(document.getElementById('title'+record.id)).value;  
        let urlValue= ReactDOM.findDOMNode(document.getElementById('url'+record.id)).value;  
        record.title= titleValue;
        record.url= urlValue;
        record.status=0;
        axios.put('http://localhost:3003/vedio/'+record.id,record)
      	.then((response) => {       
          console.log("save:"+response.data)
          this.state.getVideos();
          this.state.cancel();
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      getVideos: ()=>{
        axios.get('http://localhost:3003/vedio')
      	.then((response) => {
          let list = response.data ;
          let list1=[]
          for(let i=0; i<list.length;i++){
             if(list[i].status !== -1 ){
              list1.push(list[i]);
             }
          }
          this.state.data=list1;
          this.setState(this.state);    
        })
          .catch(function (error) {
            console.log(error);
        });
      },
			cancel: ()=>{
        this.state.columns[1].editable= false;
        this.state.columns[2].editable= false;
        this.state.columns[3].editable= true;
        this.setState(this.state)
      },
			edit: () => {    
        this.state.columns[1].editable= true;
        this.state.columns[2].editable= true;
        this.state.columns[3].editable= false;
        this.setState(this.state)
			},
			editingKey: '',
			setEditingKey: ()=>{},

		}
	}
	
	componentDidMount() {
		this.state.getVideos();
	}
	//render是一个生命周期中的函数
	render(){
        return (
              <Form >
                <Button type="primary" size="large" onClick={this.state.edit}> Edit </Button>
                <Button type="primary" size="large" onClick={this.state.cancel} style={{marginLeft: 10,}}> Cancel </Button>
                <Table 
                  rowKey={record => record.id}
                  bordered
                  dataSource={this.state.data}
                  columns={this.state.columns}
                  rowClassName="editable-row"
                />
              </Form>
		  );
	}
}
export default EditVideo


