import React, {Component, useState } from 'react';
import 'antd/dist/antd.css';
import { Table, Input, InputNumber, Popconfirm, Form } from 'antd';

import Axios from 'axios';

const axios = Axios.create();

let originData = [];

export class Editable extends Component{
	constructor(props){
		// 父类构造函数的引用
		super(props)
		// this.state是当前class组件私有数据
        this.state= {
            data: []
        }
    }
    componentDidMount() {
		axios.get('http://localhost:3003/vedio')
      	.then((response) => {
            this.state.data = response.data;
            originData= this.state.data;
            this.setState(this.state) ;
            console.log(originData);       
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    render(){
        return (
            <EditableTable />
        )}
}
export default Editable;

const EditableTable = () => {
    const [form] = Form.useForm();
    const [data, setData] = useState(originData);
    const [editingKey, setEditingKey] = useState('');
    const isEditing = record => record.key === editingKey;

    const edit = record => {
        form.setFieldsValue({
        id: '',
        title: '',
        url: '',
        ...record,
        });
        setEditingKey(record.key);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const save = async key => {
        try {
        const row = await form.validateFields();
        const newData = [...data];
        const index = newData.findIndex(item => key === item.key);

        if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, { ...item, ...row });
            setData(newData);
            setEditingKey('');
        } else {
            newData.push(row);
            setData(newData);
            setEditingKey('');
        }
        } catch (errInfo) {
        console.log('Validate Failed:', errInfo);
        }
    };

    const columns = [
        {
        title: 'id',
        dataIndex: 'id',
        width: '25%',
        editable: true,
        },
        {
        title: 'Title',
        dataIndex: 'title',
        width: '15%',
        editable: true,
        },
        {
        title: 'URL',
        dataIndex: 'url',
        width: '40%',
        editable: true,
        },
        {
        title: 'operation',
        dataIndex: 'operation',
        render: (_, record) => {
            const editable = isEditing(record);
            return editable ? (
            <span>
                <a
                href="javascript:;"
                onClick={() => save(record.key)}
                style={{
                    marginRight: 8,
                }}
                >
                Save
                </a>
                <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
                <a>Cancel</a>
                </Popconfirm>
            </span>
            ) : (
            <a disabled={editingKey !== ''} onClick={() => edit(record)}>
                Edit
            </a>
            );
        },
        },
    ];
    const mergedColumns = columns.map(col => {
        if (!col.editable) {
        return col;
        }

        return {
        ...col,
        onCell: record => ({
            record,
            inputType: col.dataIndex === 'age' ? 'number' : 'text',
            dataIndex: col.dataIndex,
            title: col.title,
            editing: isEditing(record),
        }),
        };
    });
    return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: cancel,
        }}
      />
    </Form>
  );
};

const EditableCell = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{
              margin: 0,
            }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

