  import React, { Component } from 'react'
  import { Menu } from 'antd';
  import {
    UserOutlined,
    VideoCameraOutlined  
  } from '@ant-design/icons';
  import {
    HashRouter ,
    Link,
  } from "react-router-dom";
  import Axios from 'axios';

  const axios = Axios.create();
  const { SubMenu } = Menu;


export class PlayList extends Component{
	constructor(props){
		// 父类构造函数的引用
		super(props)
		// this.state是当前class组件私有数据
		this.state= {
			videos: []
		}
  }

  componentDidMount() {
      axios.get('http://localhost:3003/vedio')
      .then((response) => {
          this.setState({
              videos: response.data
          })
      })
      .catch(function (error) {
          console.log(error);
      });
  }

	//render是一个生命周期中的函数
	render(){
        return  (    
          <Menu theme="light" 
                mode="inline"
                defaultSelectedKeys={['video1']}
                defaultOpenKeys={['videos']}
                style={{ height: '100%', borderRight: 0 }}
          >
            <Menu.Item key="edit">
              <UserOutlined />
              <HashRouter>
                    <Link to="/edit">Edit Vedios</Link>
                  </HashRouter>
            </Menu.Item>
            <SubMenu  
              key="videos"
              title={
                <span>
                 <VideoCameraOutlined />
                  Play Videos
                </span>
              }
            >
              {this.state.videos.map(video => (
                video.status === 1? 
                (<Menu.Item key={"video"+video.id}>  
                  <HashRouter>
                    <Link to={'/video/'+video.id}>{video.title}</Link>
                  </HashRouter>                             
                </Menu.Item>) : ""
              ))}
              
            </SubMenu>
            
      </Menu> )
  }
  

}

export default PlayList