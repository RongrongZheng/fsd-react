import React ,{ Suspense}from 'react';
import { Layout} from 'antd';
import 'react-fontawesome';
import { Route, Switch,Redirect,HashRouter } from 'react-router-dom';
import{ PlayList } from './component/play-list/PlayList'
import Control from './component/control/Control';
import HeaderContent from './component/header/HeaderContent';
import {EditVideo } from './component/edit-vedio/EditVideo';
import './App.css';
import 'antd/dist/antd.css';

const { Header, Content, Footer, Sider } = Layout;

const App = () => (
  <Layout>
    <Sider   theme="light" breakpoint="lg" collapsedWidth="0" >
      <div className="logo" >这里是logo</div>
      <PlayList />
      {/* <Menu theme="light" 
                mode="inline"
                defaultSelectedKeys={['video1']}
                defaultOpenKeys={['videos']}
                style={{ height: '100%', borderRight: 0 }}
          >
            <Menu.Item key="edit">
              <UserOutlined />
              <HashRouter>
                    <Link to="/edit">Edit Vedios
                    </Link>
                  </HashRouter>
            </Menu.Item>         
            
      </Menu> */}
    </Sider>
    <Layout>
      <Header className="site-layout-sub-header-background" style={{ padding: 0 }} >
        <HeaderContent />
      </Header>
      <Content style={{ margin: '24px 16px 0' }}>
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
          <HashRouter>
            <Suspense fallback={<div>Loading...</div>}>
              <Switch>               
                <Route exact path="/video/:id" component={Control}/>
                <Route exact path="/edit" component={EditVideo}/>
                <Redirect from="/" to="/edit" exact strict />
              </Switch>
            </Suspense>
          </HashRouter>  
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>@***2020 FSD React***@ </Footer>
    </Layout>
  </Layout>
);

export default App;